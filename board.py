"""a representation of a board for the tic-tac-toe game"""

class TicTacToeBoard:
    """Tic-Tac-Toe board representation"""
    
    def __init__(self):
        """Constructor"""
        self.board = ["."]*9
        
    def mark(self, symbol, position):
        """Mark a symbol at a position"""
        self.board[position - 1] = symbol
        
    def terminate(self):
        """Determine if the game is over"""
        ok = (((self.board[0] == 'x') and (self.board[1] == 'x') and (self.board[2] == 'x') or
              (self.board[3] == 'x') and (self.board[4] == 'x') and (self.board[5] == 'x') or
              (self.board[6] == 'x') and (self.board[7] == 'x') and (self.board[8] == 'x') or
              (self.board[0] == 'x') and (self.board[3] == 'x') and (self.board[6] == 'x') or
              (self.board[1] == 'x') and (self.board[4] == 'x') and (self.board[7] == 'x') or
              (self.board[2] == 'x') and (self.board[5] == 'x') and (self.board[8] == 'x') or
              (self.board[0] == 'x') and (self.board[4] == 'x') and (self.board[8] == 'x') or
              (self.board[2] == 'x') and (self.board[4] == 'x') and (self.board[6] == 'x')) or
        ((self.board[0] == 'o') and (self.board[1] == 'o') and (self.board[2] == 'o') or
         (self.board[3] == 'o') and (self.board[4] == 'o') and (self.board[5] == 'o') or
         (self.board[6] == 'o') and (self.board[7] == 'o') and (self.board[8] == 'o') or
         (self.board[0] == 'o') and (self.board[3] == 'o') and (self.board[6] == 'o') or
         (self.board[1] == 'o') and (self.board[4] == 'o') and (self.board[7] == 'o') or
         (self.board[2] == 'o') and (self.board[5] == 'o') and (self.board[8] == 'o') or
         (self.board[0] == 'o') and (self.board[4] == 'o') and (self.board[8] == 'o') or
         (self.board[2] == 'o') and (self.board[4] == 'o') and (self.board[6] == 'o')))
              
        """ok = (self.board[0] == 'x') and (self.board[1] == 'x') and (self.board[2] == 'x')"""
        return ok
    
    def winner(self):
        if self.terminate():
            if (self.board[0] == 'x') and (self.board[1] == 'x') and (self.board[2] == 'x'):
                return self.board[0]
            if (self.board[3] == 'x') and (self.board[4] == 'x') and (self.board[5] == 'x'):
                return self.board[3]
            if (self.board[6] == 'x') and (self.board[7] == 'x') and (self.board[8] == 'x'):
                return self.board[6]
            if (self.board[0] == 'x') and (self.board[3] == 'x') and (self.board[6] == 'x'):
                return self.board[0]
            if (self.board[1] == 'x') and (self.board[4] == 'x') and (self.board[7] == 'x'):
                return self.board[1]
            if (self.board[2] == 'x') and (self.board[5] == 'x') and (self.board[8] == 'x'):
                return self.board[2]
            if (self.board[0] == 'x') and (self.board[4] == 'x') and (self.board[8] == 'x'):
                return self.board[0]
            if (self.board[2] == 'x') and (self.board[4] == 'x') and (self.board[6] == 'x'):
                return self.board[2]
            if (self.board[0] == 'o') and (self.board[1] == 'o') and (self.board[2] == 'o'):
                return self.board[0]
            if (self.board[3] == 'o') and (self.board[4] == 'o') and (self.board[5] == 'o'):
                return self.board[3]
            if (self.board[6] == 'o') and (self.board[7] == 'o') and (self.board[8] == 'o'):
                return self.board[6]
            if (self.board[0] == 'o') and (self.board[3] == 'o') and (self.board[6] == 'o'):
                return self.board[0]
            if (self.board[1] == 'o') and (self.board[4] == 'o') and (self.board[7] == 'o'):
                return self.board[1]
            if (self.board[2] == 'o') and (self.board[5] == 'o') and (self.board[8] == 'o'):
                return self.board[2]
            if (self.board[0] == 'o') and (self.board[4] == 'o') and (self.board[8] == 'o'):
                return self.board[0]
            if (self.board[2] == 'o') and (self.board[4] == 'o') and (self.board[6] == 'o'):
                return self.board[2]
        return None