"""Tests for game termination conditions"""
from board import TicTacToeBoard

class TestWinByRow:
    """Test cases for win by row"""
    
    def test_row_1(self):
        """Win at top row"""
        # Position 1,2,3
        
        board = TicTacToeBoard();
        board.mark("x", 1)
        board.mark("x", 2)
        board.mark("x", 3)
        
        assert board.terminate()
        assert board.winner() == "x"
        
    def test_row_2(self):
        """Win at mid row"""
        # Position 4,5,6
        
        board = TicTacToeBoard();
        board.mark("x", 4)
        board.mark("x", 5)
        board.mark("x", 6)
        
        assert board.terminate()
        assert board.winner() == "x"
        
    def test_row_3(self):
        """Win at bottom row"""
        # Position 7,8,9
        
        board = TicTacToeBoard();
        board.mark("o", 7)
        board.mark("o", 8)
        board.mark("o", 9)
        
        assert board.terminate()
        assert board.winner() == "o"

class TestWinByColumn:
    """Test cases for win by column"""
    
    def test_col_1(self):
        """Win at left column"""
        # Position 1,4,7
        
        board = TicTacToeBoard();
        board.mark("o", 1)
        board.mark("o", 4)
        board.mark("o", 7)
        
        assert board.terminate()
        assert board.winner() == "o"
        
    def test_col_2(self):
        """Win at mid column"""
        # Position 2,5,8
        
        board = TicTacToeBoard();
        board.mark("x", 2)
        board.mark("x", 5)
        board.mark("x", 8)
        
        assert board.terminate()
        assert board.winner() == "x"
        
    def test_col_3(self):
        """Win at right column"""
        # Position 3,6,9
        
        board = TicTacToeBoard();
        board.mark("o", 3)
        board.mark("o", 6)
        board.mark("o", 9)
        
        assert board.terminate()
        assert board.winner() == "o"

class TestWinByDiagonal:
    """Test cases for win by diagonal"""
    
    def test_diag_1(self):
        """Win at main diagonal"""
        # Position 1,5,9
        
        board = TicTacToeBoard();
        board.mark("x", 1)
        board.mark("x", 5)
        board.mark("x", 9)
        
        assert board.terminate()
        assert board.winner() == "x"
        
    def test_diag_2(self):
        """Win at antidiagonal """
        # Position 3,5,7
        
        board = TicTacToeBoard();
        board.mark("o", 3)
        board.mark("o", 5)
        board.mark("o", 7)
        
        assert board.terminate()
        assert board.winner() == "o"

class TestDraw:
    
    def test_draw_1(self):
        """Test cases for draw"""
        board = TicTacToeBoard();
        board.mark("x", 1)
        board.mark("o", 2)
        board.mark("x", 3)
        board.mark("o", 5)
        board.mark("x", 8)
        board.mark("o", 6)
        board.mark("x", 4)
        board.mark("o", 7)
        board.mark("x", 9)
        
        assert board.terminate() == False
        assert board.winner() == None
    
    def test_draw_2(self):
        board = TicTacToeBoard();
        isDraw = False
        board.mark("x", 1)
        board.mark("o", 2)
        board.mark("x", 3)
        board.mark("o", 5)
        board.mark("x", 8)
        board.mark("o", 6)
        board.mark("x", 4)
        board.mark("o", 7)
        board.mark("x", 9)
        
        for i in board.board:
            if i == ".":
                isDraw = False
                break
            else:
                isDraw = True
    
        assert board.terminate() == False
        assert isDraw == True
        assert board.winner() == None
    
        
