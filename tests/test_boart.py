"""Test class TicTacToeBoard"""

from board import TicTacToeBoard

def test_contructor():
    """Construct a TicTacToeBoard object"""
    # Arrange
    
    # Act
    sample = TicTacToeBoard()
    
    # Assert
    assert len(sample.board) == 9
    assert sample.board[0] == "."
    assert sample.board[1] == "."
    assert sample.board[2] == "."
    assert sample.board[3] == "."
    assert sample.board[4] == "."
    assert sample.board[5] == "."
    assert sample.board[6] == "."
    assert sample.board[7] == "."
    assert sample.board[8] == "."

def test_mask():
    """Mark a symbol"""
    # Arrange
    sample = TicTacToeBoard()
    
    # Act
    sample.mark('o', 4)
    
    # Assert
    assert sample.board[3] == 'o'

def test_terminate():
    sample = TicTacToeBoard()
    sample.mark("o", 1)
    sample.mark("o", 2)
    sample.mark("o", 3)
    
    assert sample.terminate()
    
def test_winner():
    sample = TicTacToeBoard()
    sample.mark("o", 3)
    sample.mark("o", 5)
    sample.mark("o", 7)
    
    assert sample.winner() == 'o'